#!/bin/sh
ROOT_DIR=$(cd $(dirname $0)/.. && pwd)
CONF=$ROOT_DIR/mongod.conf

if [ ! -f $CONF ]; then
  echo "$CONF not exists."
  exit 1;
fi

mongod --config $CONF &

